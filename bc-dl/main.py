from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from urllib.request import urlretrieve
import os, sys

# Connect to web page
browser = webdriver.Chrome()
albums = [
	# Add here
	]

# Loop through albums
for album in albums:
	browser.get(album)
	
	# get list of tracks
	tracks = browser.find_elements(By.CSS_SELECTOR, "#track_table tbody tr.track_row_view")
	
	# wait a bit for all basic requests to go through
	print("Sleeping for 5 seconds")
	sleep(5)
	
	trackno = 0
	# Loop through tracks
	for track in tracks:
		trackno += 1
		
		title = track.find_element(By.CLASS_NAME, "track-title").text.replace(":", " -").replace("/", "-").replace("?", "")
		print("Downloading track",trackno,title)
		
		# Clear browser requests
		del browser.requests
		
		# Play the track
		print("Playing...")
		track.find_element(By.CLASS_NAME, "play_status").click()
		sleep(10)
		track.find_element(By.CLASS_NAME, "play_status").click()
		
		for request in browser.requests:
		    if request.response and "mp3" in request.url:
		        url = request.url
		
		print("Downloading...")
		filename, _ = urlretrieve(url, f"{title}.mp3")
		print(f"Filename: {filename}, URL: {url}")
		os.rename(f"{os.path.dirname(os.path.realpath(__file__))}/{filename}", f"{os.environ['HOME']}/Music/bc-dl/{filename}")
		print(f"Completed {title}!\n")
	print(f"Finished downloading album {album}.\n")
print("Completed all albums")